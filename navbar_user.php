<?php
  include('code/security.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/navstyle.css">
  </head>
  <body>
    <nav>
      <input type="checkbox" id="check">
      <label for="check" class="checkbtn">
        <i class="fas fa-bars"></i>
      </label>
      <label class="logo">ADMIN</label>
      <ul>
        <li><a class="active" href="u_dashboard.php">Home</a></li>
        <li><a href="code/code_logout.php">Logout</a></li>
      </ul>
    </nav>
