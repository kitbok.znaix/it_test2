<?php 
    include('navbar_admin.php');
?>
  <title>Display Other</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style/admin_display.css"/>



    <!-- <h1>Welcome <?= $_GET['name']; ?></h1> -->

    <div class="btn-container">
          <a href="a_dashboard.php"><input type="button" class="button button-back" value="Back"></a>
    </div>

    <?php if(isset($_SESSION['message'])) : ?>
                <h5><?= $_SESSION['message'] ?></h5>
                <?php
                    unset($_SESSION['message']);
                    endif; 
      ?>


   <table class="table">
     <thead>
     	<tr>
     	 <th>Name</th>
     	 <th>Phone No</th>
     	 <th>Email</th>
     	 <th>Role</th>
     	 <th colspan="2">Action</th>
     	</tr>
     </thead>
     <tbody>

     <?php
         $query = "SELECT u.id, u.name, u.phone, u.email, r.role_name FROM role r INNER JOIN users u ON u.role_no = r.id;";  //SELECT * FROM users
         $statement = $conn->prepare($query);
         $statement->execute();

                    
         $result = $statement->fetchAll(PDO::FETCH_OBJ);
                                    
         if($result) { // check record
            ?> <div class="record">
            <p> Current Record</p>
                </div>
            <?php
             foreach($result as $row) {

              ?>

     	  <tr>
     	  	  <td><?= $row->name; ?></td>
     	  	  <td><?= $row->phone; ?></td>
     	  	  <td><?= $row->email; ?></td>
     	  	  <td><?= $row->role_name; ?></td>
     	  	  <td>
                <form action="display_admin_edit.php" method="post">
                    <input type="hidden" name="submit_edit_id" value="<?= $row->id; ?>">
                    <button type="submit" id="edit" name="edit_btn">Edit</button>
                    </form>
                </td>
     	  	  <td>
                 <form action="code/code_delete_admin.php?name=<?$getname?>" method="post">
                 <input type="hidden" name="submit_delete_id" value="<?= $row->id; ?>">
                <button type="submit" id="edit" name="delete_btn">DELETE</button>
                 </form>
            </td>
     	  </tr>

           <!-- <script src="js/update_popup.js"></script> -->

     	  <?php
                        }
                    } else {
                        ?>
                        <p class="record"> NO Record Found</p>
                    <?php
                     }
                    ?>
     </tbody>
   </table>

</body>
</html>