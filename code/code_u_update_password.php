<?php
    include('security.php');
    if(isset($_POST['p_update_btn'])) {
        $user_id = $_POST['user_id'];
        $o_password = $_POST['o_password'];
        $n_password = $_POST['n_password'];
        $c_password = $_POST['c_password'];
         echo $user_id;

        try {
            //fetch password from database
            $sql = "SELECT password FROM users WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $user_id);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $existingPassword = $row['password'];

    
            //compare Old and existing password
            if (!password_verify($o_password, $existingPassword)) {
                echo "Old password is incorrect";
                exit(0);
            }

            //compare password and confirm password
            if ($n_password != $c_password) {
                $_SESSION['message'] = 'Passwords do not match.';
                header('Location: ../u_update_password.php');
                exit(0);
            }

            //hash password
            $h_password = password_hash($n_password, PASSWORD_BCRYPT);

            // start UPDATE
            $query = "UPDATE users SET password=:h_password WHERE id=:u_id";
            $statement = $conn->prepare($query);
            
            $data = [
                ':h_password' => $h_password,
                ':u_id' => $user_id
            ];
            $query_execute = $statement->execute($data);

            if($query_execute) {
                $_SESSION['message'] = "Change Successfully";
                header('location: ../u_dashboard.php');
                exit(0);
            } else {
                $_SESSION['message'] = "Error: Cannot change password";
                header('location: ../u_dashboard.php');
                exit(0);
                }

        } catch(PDOException $e) {
            echo $e->getMessage();
        }

    }
?>