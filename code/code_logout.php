<?php

session_start(); // Start or resume the session

// Destroy the session
session_destroy();

// Optionally, unset all session variables
$_SESSION = array();

// Clear cache
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Pragma: no-cache");

// Redirect to the login page
header("Location: ../login.php");
exit();

?>